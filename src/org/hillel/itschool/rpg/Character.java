package org.hillel.itschool.rpg;

public abstract class Character {
    private String name;

    public boolean isPlayable() {
        return false;
    }

    public String getName(){

        return this.name;
    }

    public void attack(Character character){

    }

    public void speak(Character character){

    }
}
